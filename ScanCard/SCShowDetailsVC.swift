//
//  SCShowDetailsVC.swift
//  ScanCard
//
//  Created by DineshReddy on 25/10/16.
//  Copyright © 2016 myprojects. All rights reserved.
//

import UIKit

class SCShowDetailsVC: UIViewController {

    @IBOutlet var buttonForDone: UIButton!
    @IBOutlet var labelForCardNum: UILabel!
    @IBOutlet var labelForExpiry: UILabel!
    @IBOutlet var labelForCVV: UILabel!
    var cardNumber: String = ""
    var cvv: String = ""
    var expiryDate: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonForDone.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.buttonForDone.layer.borderWidth = 1.0
        // Do any additional setup after loading the view.
        
        labelForCardNum.text =  NSString(format: "Card No : %@",cardNumber) as String

        labelForCVV.text =  NSString(format: "CVV : %@",cvv) as String

        labelForExpiry.text =  NSString(format: "Expiry : %@",expiryDate) as String
    }

    @IBAction func onClickDoneButton(sender: UIButton)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
