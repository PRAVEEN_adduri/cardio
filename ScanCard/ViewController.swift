//
//  ViewController.swift
//  ScanCard
//
//  Created by DineshReddy on 25/10/16.
//  Copyright © 2016 myprojects. All rights reserved.
//

import UIKit

class ViewController: UIViewController,
CardIOPaymentViewControllerDelegate, UITextFieldDelegate
{
    
    @IBOutlet var buttonForScanCard: UIButton!
    @IBOutlet var textFieldForCardNum: UITextField!
    @IBOutlet var textFieldForCVV: UITextField!
    @IBOutlet var textFieldForExpiry: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.buttonForScanCard.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.buttonForScanCard.layer.borderWidth = 1.0
        
        
        self.addToolBar(self.textFieldForCardNum)
        self.addToolBar(self.textFieldForCVV)
        self.addToolBar(self.textFieldForExpiry)
        
        self.textFieldForExpiry.addTarget(self, action: #selector(ViewController.dateTextFieldDidChange), forControlEvents: UIControlEvents.EditingChanged)
        
        self.textFieldForCVV.addTarget(self, action: #selector(ViewController.cvvTextFieldDidChange), forControlEvents: UIControlEvents.EditingChanged)

        
        self.textFieldForCardNum.addTarget(self, action: #selector(ViewController.creditCardTextFieldDidChange), forControlEvents: UIControlEvents.EditingChanged)

        
        self.textFieldForExpiry.delegate = self
        
    }

    //On Click Submit Button Action
    @IBAction func onClickSubmitbutton(sender: UIButton)
    {
        if !self.textFieldForCardNum!.text!.isEmpty && !self.textFieldForCVV!.text!.isEmpty && !self.textFieldForExpiry!.text!.isEmpty
        {
            if (self.textFieldForCardNum.text!.characters.count == 16 && self.textFieldForCVV.text!.characters.count == 3 && self.textFieldForExpiry.text!.characters.count == 5)
            {
                let showDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier("SCShowDetailsVC") as? SCShowDetailsVC
                showDetailsVC?.cardNumber = self.textFieldForCardNum.text!
                showDetailsVC?.cvv = self.textFieldForCVV.text!
                showDetailsVC?.expiryDate = self.textFieldForExpiry.text!

                self.navigationController?.pushViewController(showDetailsVC!, animated: true)
            }
            else
            {
                let alert = UIAlertView()
                alert.title = "Alert"
                if (self.textFieldForCardNum.text!.characters.count != 16)
                {
                    alert.message = "Please Enter Valid Card Number"
                }
                else if(self.textFieldForCVV.text!.characters.count != 3)
                {
                    alert.message = "Please Enter Valid CVV"
                }
                else if(self.textFieldForExpiry.text!.characters.count != 5)
                {
                    alert.message = "Please Enter Valid Expiry Date"
                }
                alert.addButtonWithTitle("OK")
                alert.show()
            }
        }
    }
    //On Click Scan Button Action

    @IBAction func onClickScanButton(sender: UIButton)
    {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC.modalPresentationStyle = .FormSheet
        presentViewController(cardIOVC, animated: true, completion: nil)
    }
    //Credit Card Validate
    func creditCardTextFieldDidChange()
    {
        let string = self.textFieldForCardNum.text! as String;
        if (string.characters.count > 16) {
            
            self.textFieldForCardNum.text = (string as NSString).substringToIndex(16)
        }
    }
    //CVV Validate
    func cvvTextFieldDidChange()
    {
        let string = self.textFieldForCVV.text! as String;
        if (string.characters.count > 3)
        {
            self.textFieldForCVV.text = (string as NSString).substringToIndex(3)
        }
    }
    //Expiry Date Validate
    func dateTextFieldDidChange()
    {
        let string = self.textFieldForExpiry.text! as String;
        let slash = "/"
        if (string.characters.count == 2){
            self.textFieldForExpiry.text = string + slash
        } else if (string.characters.count > 5) {
            self.textFieldForExpiry.text = (string as NSString).substringToIndex(5)
        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (string == "" && self.textFieldForExpiry.text?.characters.count == 3)
        {
            let dateString = self.textFieldForExpiry.text;
            self.textFieldForExpiry.text = dateString!.stringByReplacingOccurrencesOfString("/", withString: "")
        }
        return true;
    }
    // Adding ToolBar To Keyboard
    func addToolBar(textField: UITextField)
    {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(ViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
    }
    func donePressed(){
        view.endEditing(true)
    }
    func cancelPressed(){
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // CardIO Delegate Methods
    func userDidCancelPaymentViewController(paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func userDidProvideCreditCardInfo(cardInfo: CardIOCreditCardInfo!, inPaymentViewController paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@. ", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            NSLog("%@", str as String)
            
            let expiryYear = NSString(format: "%lu",info.expiryYear)
            
            let expiry_Date = NSString(format: "%02lu/%@", info.expiryMonth, (expiryYear as NSString).substringFromIndex(2))
            
            self.textFieldForCardNum.text = info.cardNumber
            self.textFieldForCVV.text = info.cvv
            self.textFieldForExpiry.text = expiry_Date as String
            
            
        }
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
}

